#include <string>
#include <sstream>
#include <vector>

using namespace std;

class display_iostream {
 private:
  int maxwidth;
  int maxheight;
  avatar *avatars;
  fieldimage *fields;
  effect *effects;

 public:
  int unitmap(vector<int> &alist, int findex);
  int matrix(int x, int y, int ox, int oy, int size, int range, int occ, int unocc, int team);
  int menu(const char *title, const char *menu, int cursor, int max);
  int message(string mesg);
  int confirm();
  int cancel();
  int units(int *units);
  int unitlist(string lists);
  int showeffect(vector<int> &alist, int findex, string elist, vector<int> &coors);
  int addavatar(char achar);
  int addfield(string glist, int *coors);
  int drawall();

  display_iostream();
  ~display_iostream();
};


display_iostream::display_iostream()
{
  maxwidth = 32;
  maxheight = 9;
  avatars = NULL;
  fields = NULL;
  effects = NULL;
}


display_iostream::~display_iostream()
{
}


int display_iostream::unitmap(vector<int> &alist, int findex){
  fieldimage *fielder=fields;
  graph *grapher;
  param *paramer;
  avatar *avater;
  int sx, sy, ax=0, ay=0, fx=0, fy=0;
  int a, n;
  int aindex, avatarnum, fieldnum=1;
  char fchar, nchar=32;

  while(fielder!=NULL && fieldnum!=findex){
    fielder=fielder->next;
    fieldnum++;
  }
  
  // Debug.
  cout << "  ";
  for(sx=1; sx*3<maxwidth; sx++){
    cout << " " << sx;              // Print x coordinates across the top of the grid.
    if(sx < 9)
      cout << " ";
  }
  cout << endl;

  for(sy=1; sy<=maxheight; sy++){
    cout << sy;                     // Debug.
    for(sx=1; sx<=maxwidth; sx++){
    
      // Get next avatar coordinates.
      if(ax<sx || ay<sy){
        a=0;
        aindex=0;
        ax = maxwidth+1;
        ay = maxheight+1;

        while(a < alist.size()){
          if(a+1 < alist.size()  &&  a+2 < alist.size()  &&  alist.at(a+1) >= sx  &&  alist.at(a+2) >= sy)
            if(alist.at(a+1) <= ax  &&  alist.at(a+2) <= ay){
              aindex = alist.at(a);
              ax = alist.at(a+1);
              ay = alist.at(a+2);
            }

          for(n=1; n<=3; n++)   // Advance to next avatar.
            if(a < alist.size())
              a++;
        }
        if(ax > maxwidth  &&  ay > maxheight){
          ax=0;
          ay=0;
        }
      }

      // Get next field image coordinates.
      if(fx<sx || fy<sy){
        if(fielder!=NULL){
          grapher=fielder->statics;
          fchar=nchar;
          fx=maxwidth+1; fy=maxheight+1;
          while(grapher!=NULL){
            paramer=grapher->parameters;
            while(paramer!=NULL){
              if(paramer->one >= sx  &&  paramer->two>=sy)
                if(paramer->one <= fx  &&  paramer->two <= fy){
                  fchar=grapher->image;
                  fx=paramer->one;
                  fy=paramer->two;
                }
              paramer=paramer->next;
            }
            grapher=grapher->next;
          }
        }
        if(fx>maxwidth && fy>maxheight){
          fx=0;
          fy=0;
        }
      }

      // Print out character.
      if(sx==ax && sy==ay){
        avater=avatars;
        avatarnum=1;
        while(avater!=NULL && avatarnum!=aindex){
          avater=avater->next;
          avatarnum++;
        }
        if(avatarnum==aindex)
          cout << avater->image;
        else
          cout << " ";
      }else if(sx==fx && sy==fy)
        cout << fchar;
      else
        cout << " ";
    }
    cout << endl;
  }
}


// Deprecated.
/*int display_iostream::matrix(int x, int y, int ox, int oy, int size, int range, int occ, int unocc, int team)
{
  int sx, sy;
  int dist, distx, disty;
  int ally;
  char nick[8];

  int matrix[size][size];
  int init=1;

  for(sy=0; sy<size; sy++){
    for(sx=0; sx<size; sx++){
      matrix[sx][sy] = 0;              // Change all elements to 0;
    }
  }

  while(init1.getxy(init, &sx, &sy)){
    if(sx>=ox && sy>=oy)
      if(sx < ox+size  &&  sy < oy+size)
	matrix[sx-ox][sy-oy] = init;     // Put units on the matrix.
    init++;
  }

  cout << "  ";
  for(sx=0; sx<size; sx++){
    cout << " " << sx+ox;              // Print x coordinates across the top of the grid.
    if(sx+ox < 9)
      cout << " ";
  }
  cout << endl;

  for(sy=0; sy<size; sy++){
    cout << sy+oy;
    if(sy < 10)
      cout << " ";
    for(sx=0; sx<size; sx++){

      init=matrix[sx][sy];

      //dist = distance(x-ox, y-oy, sx, sy);

      if(init==0)
	if(unocc==0 && dist<=range)
	  cout << "[ ]";
	else
	  cout << "   ";
      else if(init1.getteam(init) == team)
	if(occ==1 && dist<=range)
	  cout << "[" << init << "]";
	else
	  cout << " " << init << " ";
      else if(init1.getteam(init) != team)
	if(occ==-1 && dist<=range)
	  cout << "[" << init << "]";
	else
	  cout << " " << init << " ";
      else
	cout << " " << init << " ";
    }
    cout << endl;
  }
}*/


int display_iostream::menu(const char *title, const char *menu, int cursor, int max)
{
  char single;
  char string[64];
  int line=1, l=0;

  cout << "[" << title << "]" << endl;
  /*while(strcmp(&menu[l], "\0")!=0){
    if(strcmp(&menu[l], "\n")!=0){
      cout << line << ": ";

      //while(strcmp(&menu[l], "\n")!=0 && strcmp(&menu[l], "\0")!=0){
        single = menu[l];
        cout << single;
        l++;
      //}

      cout << endl;
      line++;
      if(strcmp(&menu[l], "\n")==0)
        l++;
    }
  }*/
  cout << menu;

  cout << ":";

  return 0;
}


int display_iostream::message(string mesg)
{
  cout << mesg;

  return 0;
}


int display_iostream::confirm()
{
}


int display_iostream::units(int *units)
{
  unit *browser;   //Pointer to browse list.
  st *finder;    //Pointer to unit stat.

  int rows=3;      //How many rows to display at once.
  int cols=6;      //How many columns to display per row.
  int lines=8;     //How many lines per listing.
  int length=10;   //How long a listing may be.

  int row = 0;     //Row integer for display.
  int col = 0;     //Column integer for display.
  int line = 0;    //Line integer for display.
  int stat = 0;    //Stat integer for display. 
  int n;
  int u = 0;
  int ten=10;
  int len=5;
  int init = 1;    //Initiative number.

  browser=start;   //Point *browser to first unit.
  while(browser!=NULL){
    for(row=0; row<=rows; row++)
      if(browser!=NULL)
	for(line=1; line<=lines; line++){
	  browser=start;
	  init=1;
	  for(col=1; col<=cols; col++){
	    if(units[u] != 0)
	      u = ((row*cols)+col) - 1;
	    while(init != units[u]  &&  units[u] > 0  ||  init != (row*cols)+col  &&  units[u] == 0){
	      //cout << "(row*cols)+col: " << row*cols+col << "   ";
	      //cout << "u: " << u << "  units[u]: " << units[u] << "   ";
	      //cout << "init: " << init << "   ";
	      if(init > units[u])
		if(init > (row*cols)+col  ||  units[u] > 0){
		  browser=start;
		  init=1;
		  //cout << "init=1";
		}
	      if(init < (row*cols)+col  &&  units[u] == 0  ||  init < units[u]  &&  units[u] != 0){
   		  if(browser!=NULL)
		    browser=browser->next;
		  init++;
		  //cout << "init++";
	      }
	      //cout << "browser: " << browser << endl;
	    }
	    if(browser!=NULL){
	      if(line==1)
		cout << "Init: " << init << "   ";
	      else if(line==2)
                cout << "Nick: " << browser->title << "   ";
	      else if(line==3)
		cout << "Pos : " << browser->x << "," << browser->y << " ";
	      else if(line==4)
		cout << "Team: " << browser->team << "   ";
	      else{
		finder=browser->stats;
		for(stat=0; stat < line-4; stat++)
		  if(finder!=NULL)
		    finder=finder->next;
		if(finder!=NULL){
		  cout << finder->title[0] << finder->title[1] << finder->title[2];
		  cout << /*finder->title <<*/ ": " << finder->value[1] << "/" << finder->value[2];
		  cout << "  ";
		}

	      }
	    }
	    if(units[u] > 0)
	      u++;
	    if(units[u] < 0)
	      browser=NULL;
	  }
	  cout << endl;
	}
  }
}


int display_iostream::unitlist(string list)
{
  int rows=3;      //How many rows to display at once.
  int cols=6;      //How many columns to display per row.
  int lines=5;    //How many lines per listing.
  int length=13;   //How long a listing may be.

  int row = 0;     //Row integer for display.
  int col = 0;     //Column integer for display.
  int line = 0;    //Line integer for display.
  int stat = 0;    //Stat integer for display.

  char output[length+1];
  output[length] = 0;
  char blank = 32;        // Blank char for empty space.
  int n, o, l=0, l2, unitend;
  int u = 0;
  int ten=10;
  int len=5;
  int init = 1;    //Initiative number.
  
  while(l < list.length()){
    for(row=0; row<rows; row++)
      if(l < list.length())
        for(line=1; line<=lines; line++){
          l=0;
          l2=0;
          init=1;

          for(col=1; col<=cols; col++){

            while(init < (row*cols)+col){
              l = list.find("*", l);  // Find next unit.
              if(l != string::npos)
                l++;
              else
                l = list.length();
              init++;
            }

            if(l < list.length()){
              unitend = list.find("*", l);  // Mark end of unit.
              stat=0;
              while(stat<line){
                l = list.find("\"", l);
                stat++;
                if(l != string::npos)
                  l++;                      // Move into quotes.
                if(stat<line){
                  l = list.find("\"", l);
                  if(l != string::npos)
                    l++;
                }
              }
  
              if(stat==line){
                for(o=0; o<length; o++)
                  output[o] = blank;                  // Fill output with blank space.
                l2 = list.find("\"", l);
  
                if(l != string::npos  &&  l < unitend){
                  if(l2 == string::npos  &&  l+length > list.length())
                    list.copy(output, list.length() - l, l);
                  else if(l2-l > length)
                    list.copy(output, length, l);
                  else
                    list.copy(output, l2-l, l);
                }
  
                cout << output;
              } // End stat==line If.
  
              l = list.find("\"", l);   // Move to and past end quotes.
              if(l != string::npos)
                l++;
            }

          }   // Loop col For.
        cout << endl;
        }   // Loop line For.
    // Loop row For.
  }
}


int display_iostream::showeffect(vector<int> &alist, int findex, string elist, vector<int> &coors){
  fieldimage *fielder=fields;
  graph *grapher;
  param *paramer;
  avatar *avater;
  int sx, sy, ax=0, ay=0, fx=0, fy=0, ex=0, ey=0;
  int a, e=0, c=0, n;
  int aindex, avatarnum, fieldnum=1;
  char fchar, echar, nchar=32;

  while(fielder!=NULL && fieldnum!=findex){
    fielder=fielder->next;
    fieldnum++;
  }

  // Debug.
  cout << "  ";
  for(sx=1; sx*3<maxwidth; sx++){
    cout << " " << sx;              // Print x coordinates across the top of the grid.
    if(sx < 9)
      cout << " ";
  }
  cout << endl;

  for(sy=1; sy<=maxheight; sy++){
    cout << sy;                     // Debug.
    for(sx=1; sx<=maxwidth; sx++){
    
      // Get next avatar coordinates.
      if(ax<sx || ay<sy){
        a=0;
        aindex=0;
        ax = maxwidth+1;
        ay = maxheight+1;

        while(a < alist.size()){
          if(a+1 < alist.size()  &&  a+2 < alist.size()  &&  alist.at(a+1) >= sx  &&  alist.at(a+2) >= sy)
            if(alist.at(a+1) <= ax  &&  alist.at(a+2) <= ay){
              aindex = alist.at(a);
              ax = alist.at(a+1);
              ay = alist.at(a+2);
            }

          for(n=1; n<=3; n++)   // Advance to next avatar.
            if(a < alist.size())
              a++;
        }
        if(ax > maxwidth  &&  ay > maxheight){
          ax=0;
          ay=0;
        }
      }

      // Get next field image coordinates.
      if(fx<sx || fy<sy){
        if(fielder!=NULL){
          grapher=fielder->statics;
          fchar=nchar;
          fx=maxwidth+1; fy=maxheight+1;
          while(grapher!=NULL){
            paramer=grapher->parameters;
            while(paramer!=NULL){
              if(paramer->one >= sx  &&  paramer->two>=sy)
                if(paramer->one <= fx  &&  paramer->two <= fy){
                  fchar=grapher->image;
                  fx=paramer->one;
                  fy=paramer->two;
                }
              paramer=paramer->next;
            }
            grapher=grapher->next;
          }
        }
        if(fx>maxwidth && fy>maxheight){
          fx=0;
          fy=0;
        }
      }

      // Get next effect coordinates.
      if(ex<sx || ey<sy){
        e=0;
        c=0;
        echar=nchar;
        ex=maxwidth+1;
        ey=maxheight+1;

        while(c < coors.size()  &&  c+1 < coors.size()){
          if(coors.at(c) >= sx  &&  coors.at(c+1) >= sy){
            if(coors.at(c) <= ex  &&  coors.at(c+1) <= ey){
              echar = (char)elist.at(e);
              //echar = elist[e];
              ex = coors.at(c);
              ey = coors.at(c+1);
            }
          }

          for(n=1; n<=2; n++)
            if(c < coors.size())
              c++;

          if(e+1 < elist.length())
            e++;
        }
        if(ex>maxwidth && ey>maxheight){
          ex=0;
          ey=0;
        }
      }
      
      // Print out character.
      if(sx==ex && sy==ey)
        cout << echar;
      else if(sx==ax && sy==ay){
        avater=avatars;
        avatarnum=1;
        while(avater!=NULL && avatarnum!=aindex){
          avater=avater->next;
          avatarnum++;
        }
        if(avatarnum==aindex)
          cout << avater->image;
        else
          cout << " ";
      }else if(sx==fx && sy==fy)
        cout << fchar;
      else
        cout << " ";
    }  
    cout << endl;
  }
}


int display_iostream::addavatar(char achar)
{
  //cout << achar;
    
  avatar *avater;
  int avatarnum=1;

  avater=avatars;
  if(avater!=NULL){
    while(avater->next!=NULL){
      avater=avater->next;
      avatarnum++;
    }
    avater->next=new avatar;
    avater=avater->next;
    avatarnum++;
  }else{
    avater=new avatar;
    avatars=avater;
  }
  avater->next=NULL;  // So avater won't access empty avatar next time.

  if(avater!=NULL){
    avater->image=achar;
    return avatarnum;
  }else{
    cout << "!! Error: failed to allocate new memory.";
    return -1;
  }
}


int display_iostream::addfield(string glist, int *coors)
{
  fieldimage *fielder, *fieldter;
  graph *grapher, *graphter;
  param *paramer, *paramter;
  int fieldnum=1;
  int g=0, c=0;

  fielder=fields;
  if(fielder!=NULL){
    while(fielder->next != NULL){
      fieldter=fielder;
      fielder=fielder->next;
      fieldnum++;
    }

    fielder->next=new fieldimage;
    fieldter=fielder;
    fielder=fielder->next;
    fieldnum++;
  }else{
    fields=new fieldimage;
    fielder=fields;
  }
  
  if(fielder!=NULL){
    fielder->next=NULL;
    fielder->statics=new graph;
    grapher=fielder->statics;
    grapher->next=NULL;

    while(grapher!=NULL && coors[c]>-1 && coors[c+1]>-1){
      if(g < glist.length()){
        grapher->image = glist.at(g);
        g++;
        grapher->parameters=new param;
        paramer=grapher->parameters;
      }else{
        paramter=paramer;
        paramer->next=new param;
        paramer=paramer->next;
      }

      if(paramer!=NULL){
        paramer->next=NULL;
        paramer->one=coors[c];
        paramer->two=coors[c+1];
        c+=2;
      }

      if(g < glist.length()  &&  grapher!=NULL){
        graphter=grapher;
        grapher->next=new graph;
        grapher=grapher->next;
        grapher->next=NULL;
      }
    }
                                                         // LoopWhile grapher/coors.
    return fieldnum;
  }else                                                  // ElseIf fielder.
    return 0;
}


int display_iostream::drawall()
{
}
