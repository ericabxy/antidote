class control_iostream {
 public:
  int input(int *input, int max);
  int confirm();
  int cancel();

  control_iostream();
  ~control_iostream();
};


control_iostream::control_iostream()
{
}


control_iostream::~control_iostream()
{
}


int control_iostream::input(int *input, int max)
{
  int var;

  cin >> var;

  if(var <= max || max==0){
    *input = var;
    return var;
  }

  return (int)input;
}


int control_iostream::confirm()
{
  return 0;
}


int control_iostream::cancel()
{
  return -1;
}
