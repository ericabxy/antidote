int beforea=0;
int controla=1;
int aftera=2;
int idlea=3;
int cancela=4;


menuitem *menu::loadmenu(string filename)
{
  // menuitem variables.
  string title;
  string text;
  int confirm;
  int continuity;
  int max;

  // menuaction variables.
  string action;
  vector<int> num;    num.assign(9, 0);
  vector<string> str; str.assign(9, "");
  vector<int> lit;    lit.assign(9, 0);
  vector<int> vec1, vec2;
  string record;

  int cstrsize = 1024;
  char menucstr[cstrsize];
  char delim;
  char blank = 32;
  char endline = (char)"\n";

  string menu;
  int m = 0;
  int m2 = 0;
  string output;

  int t=0, t2;
  int b, b2;
  int r, r2;
  int a=string::npos, a2=string::npos;

  ifstream menufile("menu.mnu");
  menufile.get(menucstr, cstrsize, delim);
  menu=menucstr;

  string readint;
  int readerint;
  string readerstring;
  vector<int> readervector;
  int par, vec;
  int n;

  
  while(t < menu.length()){
    t = menu.find_first_not_of(" \n", t);
    t2 = menu.find_first_of(" \n", t);

    if(t != string::npos){
      if(menu.compare(t, t2-t, "TITLE") == 0  ||  menu.compare(t, t2-t, "TEXT") == 0){
                                                    // Find string and enclose in b-b2 brackets.
        b = menu.find("\"", t);
        b2 = menu.find("\"", b+1);
        r = b;
                
      }else if(menu.compare(t, t2-t, "MAX") == 0){  // Find integer and enclose in b-b2 brackets.
        b = menu.find_first_of(" \n", t);
        b = menu.find_first_not_of(" \n", b);
        b2 = menu.find_first_of(" \n", b);
        r = b;
        
      }else if(menu.compare(t, t2-t, "BEFORE") == 0  ||  menu.compare(t, t2-t, "CONTROL") == 0  ||
               menu.compare(t, t2-t, "AFTER")  == 0  ||  menu.compare(t, t2-t, "CANCEL")  == 0  ||
               menu.compare(t, t2-t, "IDLE")   == 0){
        if(a == string::npos){
          if(a2 == string::npos)
            a2 = menu.find_first_not_of(" \n", t2) - 1;

          if(a2 != string::npos  &&  a2 != menu.length())
            a = menu.find_first_not_of(" \n", a2+1);

          if(a != string::npos){
            if(menu.substr(a, 1) == "["){
              a2 = menu.find("]", a);
              if(a2 == string::npos)
                a2 = menu.length();
              b2=a;
              vec=1;
              par=1;
              cout << menu.substr(t, t2-t) << menu.substr(a, a2-a+1) << endl;
            }else if(menu.substr(a, 1) == "'"){
              a2 = menu.find("'", a+1);
              if(a2 == string::npos)
                a2 = menu.length();
              b2=a;
              cout << menu.substr(t, t2-t) << menu.substr(a, a2-a+1) << endl;
            }else{
              t=a;
              a = string::npos;
              a2 = string::npos;
              r = string::npos;
            }
          }else{
            t = string::npos;
            a = string::npos;
            a2 = string::npos;
            r = string::npos;
          }
        }

        if(a != string::npos){
          b = menu.find_first_not_of(" \n", b2+1);
          if(b != string::npos){
            if(menu.substr(b, 1) == "\""  ||  menu.substr(b, 1) == "'"){
              b2 = menu.find(menu.at(b), b+1);
            }else if(menu.substr(b, 1) == "("){
              b2 = menu.find(")", b);
            }else{
              b2 = menu.find_first_of(" \n", b) - 1;
            }

            if(b2 >= a2)
              b2 = a2-1;
            r=b;
            if(b2 < b){
              // Output last action.
              cout << "  action:" << action << endl;
              cout << "  num:";
              for(n=1; n<9; n++)
                cout << num.at(n) << " ";
              cout << endl;
              cout << "  str:";
              for(n=1; n<9; n++)
                cout << "\"" << str.at(n) << "\" ";
              cout << endl;
              cout << "  lit:";
              for(n=1; n<9; n++)
                cout << lit.at(n) << " ";
              cout << endl;
              cout << "  vec1:";
              for(n=1; n<vec1.size(); n++)
                cout << vec1.at(n) << " ";
              cout << endl;
              cout << "  vec2:";
              for(n=1; n<vec2.size(); n++)
                cout << vec2.at(n) << " ";
              cout << endl;
              cout << "  record:" << record << endl;
              
              a = string::npos;
              r = string::npos;
            }else
              cout << menu.substr(b, 0);
          }
        }

      }else{
        r = string::npos;

      }
    }


    if(r != string::npos)
      if(menu.substr(r, 1) == "\""  ||  menu.substr(r, 1) == "'"  ||  menu.substr(r, 1) == "*"){
        if(menu.substr(r, 1) == "*")
          readerstring.assign(menu, b+1, b2-b);
        else
          readerstring.assign(menu, b+1, b2-b-1);
      }else if(menu.substr(r, 1) == "("){

      }else{
        readerstring.assign(menu, b, b2-b+1);
        readerint = atoi(readerstring.c_str());
      }


    if(t != string::npos){
      if(menu.compare(t, t2-t, "TITLE") == 0){
        title = readerstring;
        t = t2;
      }else if(menu.compare(t, t2-t, "TEXT") == 0){
        text = readerstring;
        t = t2;
      }else if(menu.compare(t, t2-t, "MAX") == 0){
        max = readerint;
        t = t2;
      }else{
        if(menu.compare(t, t2-t, "BEFORE")  == 0  ||
           menu.compare(t, t2-t, "CONTROL") == 0  ||
           menu.compare(t, t2-t, "AFTER")   == 0  ||
           menu.compare(t, t2-t, "CANCEL")  == 0  ||
           menu.compare(t, t2-t, "IDLE")    == 0){

          if(a != string::npos  &&  menu.substr(a, 1) == "["){
            if(menu.substr(b, 1) == "*"){
              action = readerstring;
            }else if(menu.substr(b, 1) == "\""){
              str.at(par) = readerstring;
              par++;  // Move to next parameter.
            }else if(menu.substr(b, 1) == "'"){
              str.at(par) = readerstring;
              lit.at(par) = -1;
              par++;  // Move to next parameter.
            }else if(menu.substr(b, 1) == "("){
              if(vec==1)            // Action vectors.
                vec1 = readervector;
              else if(vec==2)
                vec2 = readervector;
              vec++;  // Move to next vector.
            }else{
              num.at(par) = readerint;
              par++;
            }
          }else if(a != string::npos  &&  menu.substr(a, 1) == "'"){
            record = readerstring;  // Name of record variable.
          }            
        }else
          t=t2;
      }
    }
  }

  return 0;
}



menuitem *menu::makemenu(string title)
{
  menumaker=new menuitem;
  tempmenu=menumaker;

  if(menumaker!=NULL){
    menumaker->before=NULL;
    menumaker->control=NULL;
    menumaker->after=NULL;
    menumaker->idle=NULL;
    menumaker->cancel=NULL;
    menumaker->newmenu=NULL;
    menumaker->next=NULL;
    menumaker->empty=0;

    cout << menumaker << menumaker->empty << endl;
   
    return menumaker;
  }else
    return 0;
}


int menu::additem(string title, string text, int continuity, int confirm, int max, menuitem *newmenu)
{
  if(menumaker!=NULL){
    if(menumaker->empty!=0){
      menumaker->next=new menuitem;
      menumaker=menumaker->next;
      if(menumaker!=NULL){
        menumaker->before=NULL;
        menumaker->control=NULL;
        menumaker->after=NULL;
        menumaker->idle=NULL;
        menumaker->cancel=NULL;
        menumaker->newmenu=newmenu;
        menumaker->next=NULL;
        menumaker->empty=0;

        //cout << menumaker << menumaker->empty << endl;
      }else
        return -1;
    }

    if(menumaker!=NULL){
      menumaker->title=title;
      menumaker->text=text;

      if(continuity > 2)
        continuity = 2;
      else if(continuity < -1)
        continuity = -1;
      menumaker->continuity=continuity;

      menumaker->confirm=confirm;

      if(max<0)
        max=0;
      menumaker->max=max;

      menumaker->newmenu=newmenu;
      
      menumaker->empty=-1;

      /*menumaker=tempmenu;
      while(menumaker!=NULL){
        cout << menumaker << menumaker->title << menumaker->text << menumaker->continuity << menumaker->confirm << menumaker->max << menumaker->empty << menumaker->next << endl;
        if(menumaker->next!=NULL)
          menumaker=menumaker->next;
        else
          break;
      }*/
    }else
      return -1;
  }

  return 0;
}


int menu::actionlist(int list)
{
  if(menumaker!=NULL){
    if(list==beforea){
      if(menumaker->before==NULL){
        menumaker->before=new menuaction;
        if(menumaker->before!=NULL)
          menumaker->before->next=NULL;
      }
      actionmaker=menumaker->before;
    }
    if(list==controla){
      if(menumaker->control==NULL){
        menumaker->control=new menuaction;
        if(menumaker->control!=NULL)
          menumaker->control->next=NULL;
      }
      actionmaker=menumaker->control;
    }
    if(list==aftera){
      if(menumaker->after==NULL){
        menumaker->after=new menuaction;
        if(menumaker->after!=NULL)
          menumaker->after->next=NULL;
      }
      actionmaker=menumaker->after;
    }
    if(list==idlea){
      if(menumaker->idle==NULL){
        menumaker->idle=new menuaction;
        if(menumaker->idle!=NULL)
          menumaker->idle->next=NULL;
      }
      actionmaker=menumaker->idle;
    }
    if(list==cancela){
      if(menumaker->cancel==NULL){
        menumaker->cancel=new menuaction;
        if(menumaker->cancel!=NULL)
          menumaker->cancel->next=NULL;
      }
      actionmaker=menumaker->cancel;
    }
  }else{
    return -1;
  }

  if(actionmaker!=NULL)
    while(actionmaker->next!=NULL)
      actionmaker=actionmaker->next;
}


int menu::addaction(string title, vector<int> num, vector<string> str, vector<int> literal, vector<int> vec1, string title1, vector<int> vec2, string title2, string record)
{
  if(actionmaker!=NULL)
    if(actionmaker->title!=""){
      actionmaker->next=new menuaction;
      actionmaker=actionmaker->next;
    }

  if(actionmaker!=NULL){
    actionmaker->title=title;
    actionmaker->num.resize(9);
    actionmaker->str.resize(9);
    actionmaker->literal.resize(9, 0);
    actionmaker->num=num;
    actionmaker->str=str;
    actionmaker->literal=literal;
    actionmaker->vec1=vec1;
    actionmaker->title1=title1;
    actionmaker->vec2=vec2;
    actionmaker->title2=title2;
    actionmaker->num.resize(9);
    actionmaker->str.resize(9);
    actionmaker->literal.resize(9, 0);
    actionmaker->record=record;
    actionmaker->next=NULL;
  }
}
