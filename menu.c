#include <vector>
#include <string>

using namespace std;


class menu {
  public:
    menu();
    ~menu();
    
    menuitem *addmenu(menuitem *addmenu);
    int run();
    int domenu(menuitem *rootmenu);
    int testmenu(menuitem *rootmenu);
    menuitem *getmenu();

    // Menumaker functions.
    menuitem *loadmenu(string filename);
    menuitem *makemenu(string title);
    int additem(string title, string text, int continuity, int confirm, int max, menuitem *newmenu);
    int actionlist(int list);
    int addaction(string title, vector<int> num, vector<string> str, vector<int> literal, vector<int> vec1, string title1, vector<int> vec2, string title2, string record);
  
  private:
    variable *variables;
    init initlist;
    display_iostream display;
    control_iostream control;
    util tools;
    menuitem *rootmenu;
    menuitem *menumaker;
    menuaction *actionmaker;
    menuitem *tempmenu;
        

    int resetmenu(menuitem *rootmenu);

    int doaction(menuaction *actions);
    int doactions(menuaction *actions);

    int setnum(string title, int num);
    int setvec(string title, vector<int> vec);
    int addtovec(string title, int num);
    int clearvec(string title);
    int setstr(string title, string str);
    int getnum(string title);
    string getstr(string title);
    vector<int> getvec(string title);
};


menu::menu(){
  display_iostream display;
  control_iostream control;
  init initlist;
  util tools;

  variables=NULL;
  rootmenu=NULL;
  menumaker=NULL;
}
menu::~menu(){
}


// Sets provided menu as root menu for this menu system.
menuitem *menu::addmenu(menuitem *addmenu)
{
  rootmenu=addmenu;
}


// Executes root menu.
int menu::run()
{
  domenu(rootmenu);
}


// Performs single menu action. Designed for CONTROL actions
int menu::doaction(menuaction *actions)
{
  //cout << "doaction\n";

  menuaction *acter;

  vector<int> num(9);
  vector<string> str(9);
  vector<int> vec1;
  vector<int> vec2;

  vector<int> unitmap;
  vector<int> targets;

  int n, recordnum;
  string title;

  acter=actions;
  while(acter!=NULL){
    for(n=0; n<=8; n++){
      if(acter->literal.at(n) != 0){
        num.at(n) = getnum(acter->str.at(n));
        str.at(n) = getstr(acter->str.at(n));
      }else{
        num.at(n) = acter->num.at(n);
        str.at(n) = acter->str.at(n);
      }
    }
    // Get vectors from action or from variables.
    if(acter->title1=="")
      vec1=acter->vec1;
    else
      vec1=getvec(acter->title1);
    if(acter->title2=="")
      vec2=acter->vec2;
    else
      vec2=getvec(acter->title2);


    title=acter->title;
    if(title=="showmenu")
      recordnum=display.menu(str.at(1).c_str(), str.at(2).c_str(), num.at(3), num.at(4)) + 1;

    else if(title=="showmessage")
      recordnum=display.message(str.at(1).c_str()) + 1;

    else if(title=="showeffect")
      recordnum=display.showeffect(unitmap, num.at(2), str.at(3), targets) + 1;
    
    else if(title=="input")
      recordnum=control.input(&num.at(1), num.at(2));

    else if(title=="gettargets")
      recordnum=tools.gettargets(targets, num.at(2), num.at(3), num.at(4), num.at(5), num.at(6), num.at(7), num.at(8), initlist) + 1;

    else if(title=="inrange")
      recordnum=tools.inrange(num.at(1), num.at(2), num.at(3), num.at(4), num.at(5)) + 1;
    
    else if(title=="findunit")
      recordnum=initlist.findunit(str.at(1).c_str(), num.at(2), num.at(3));

    else if(title=="setstr")
      recordnum=setstr(str.at(1), str.at(2)) + 1;

    else
      cout << "invalid menuaction";

    if(acter->record != "")
      setnum(acter->record, recordnum);

    if(recordnum > 0)
      acter=acter->next;
    else
      return 0;
  }

  return recordnum; // Return 1 if there is no action.
}


// Performs multiple menu actions.
int menu::doactions(menuaction *actions)
{
  cout << "doactions\n";

  menuaction *acter;

  vector<int> num(9);
  vector<string> str(9);
  vector<int> vec1;
  vector<int> vec2;

  vector<int> unitmap;     // Stores coordinates for displaying units.
  vector<int> targets;     // Stores coordinates for displaying effects.
  string unitlist;
  int x, y;

  int n, recordnum;
  string title;

  acter=actions;
  while(acter!=NULL){
    cout << acter->title << endl;
    for(n=0; n<=8; n++){
      if(acter->literal.at(n) != 0){
        num.at(n) = getnum(acter->str.at(n));
        str.at(n) = getstr(acter->str.at(n));
      }else{
        num.at(n) = acter->num.at(n);
        str.at(n) = acter->str.at(n);
      }
    }
    // Get action vectors. If title is a string, get vector from variable.
    if(acter->title1=="")
      vec1=acter->vec1;
    else
      vec1=getvec(acter->title1);
    if(acter->title2=="")
      vec2=acter->vec2;
    else
      vec2=getvec(acter->title2);


    title=acter->title;
    //cout << acter->title << endl;
    if(title=="input")
      recordnum=control.input(&num.at(1), num.at(2));

    else if(title=="getunitlist")
      recordnum=initlist.getunits(vec1, str.at(2), unitlist);

    else if(title=="getunitmap")
      recordnum=tools.getmap(unitmap, initlist);

    else if(title=="gettargets")
      recordnum=tools.gettargets(targets, num.at(2), num.at(3), num.at(4), num.at(5), num.at(6), num.at(7), num.at(8), initlist);
        
    else if(title=="showmenu")
      recordnum=display.menu(str.at(1).c_str(), str.at(2).c_str(), num.at(3), num.at(4));

    else if(title=="showmessage")
      recordnum=display.message(str.at(1).c_str());

    else if(title=="showunitlist")
      recordnum=display.unitlist(unitlist);

    else if(title=="showunitmap")
      recordnum=display.unitmap(unitmap, num.at(2));

    else if(title=="showeffect")
      recordnum=display.showeffect(unitmap, num.at(2), str.at(3), targets);

    else if(title=="getxy"){
      recordnum=initlist.getxy(num.at(1), &x, &y);
      setnum("yourx", x);
      setnum("youry", y);
    }

    else if(title=="addunit")
      recordnum=initlist.addunit(num.at(1), str.at(2).c_str(), num.at(3), num.at(4), num.at(5), num.at(6));

    else if(title=="addstat")
      recordnum=initlist.addstat(num.at(1), str.at(2).c_str(), num.at(3), num.at(4), num.at(5));

    else if(title=="findunit")
      recordnum=initlist.findunit(str.at(1).c_str(), num.at(2), num.at(3));
    
    else if(title=="getstat")
      recordnum=initlist.getstat(num.at(1), str.at(2).c_str(), num.at(3));

    else if(title=="modstat")
      recordnum=initlist.modstat(num.at(1), str.at(2).c_str(), num.at(3), num.at(4), num.at(5));
    
    else if(title=="move")
      recordnum=initlist.move(num.at(1), num.at(2), num.at(3));

    else if(title=="setnum")
      recordnum=setnum(str.at(1), num.at(2));

    else if(title=="addtovec")
      recordnum=addtovec(str.at(1).c_str(), num.at(2));

    else if(title=="clearvec")
      recordnum=clearvec(str.at(1).c_str());

    else
      cout << "invalid menuaction";

    if(acter->record != "")
      setnum(acter->record, recordnum);

    acter=acter->next;
  }

  return 0;
}


//
int menu::resetmenu(menuitem *rootmenu)
{
  //cout << "resetmenu\n";

  menuitem *menuer;

  menuer=rootmenu;
  while(menuer!=NULL){
    menuer->selection=0;
    menuer=menuer->next;
  }

  return 0;
}


int menu::domenu(menuitem *rootmenu)
{
  cout << "domenu\n";
  cout << rootmenu->title << endl;

  menuitem *thismenu, *nextmenu;
  int exit=-1;
  int place=1, dest=1;

  nextmenu=rootmenu;
  thismenu=NULL;
  while(exit!=0 && nextmenu!=NULL){
    //cout << "menuloop\n";
    while(place<dest && nextmenu!=NULL){
      if(nextmenu->continuity==proceed)
        dest = dest+nextmenu->max;
      nextmenu=nextmenu->next;
      place++;
    }

    if(place==dest && nextmenu!=NULL){
      if(nextmenu->selection > 0){
        if(thismenu!=NULL){
          //cout << "idle\n";
          doactions(thismenu->idle);
        }
        thismenu=nextmenu;
        dest = place+thismenu->selection;
      }else{
        if(thismenu==NULL)
          thismenu=nextmenu;
        cout << "before\n";
        doactions(thismenu->before);
        cout << "control\n";
        thismenu->cursor = doaction(thismenu->control);

        if(thismenu->cursor < 0)
          thismenu->cursor = 0;
        else if(thismenu->cursor > thismenu->max)
          thismenu->cursor = thismenu->max;

        if(thismenu->confirm==0 && control.confirm()==0 || thismenu->confirm!=0){
          thismenu->selection = thismenu->cursor;

          if(thismenu->selection > 0){
            cout << "after\n";
            doactions(thismenu->after);
            
            if(thismenu->continuity == proceed){
              //cout << "proceed\n";
              dest = place+thismenu->selection;
              nextmenu=thismenu->next;
              thismenu=NULL;
              place++;
            }else if(thismenu->continuity == restart){
              //cout << "restart\n";
              resetmenu(rootmenu);
              thismenu=rootmenu;
              place=1;
              dest=1;
            }else if(thismenu->continuity == newmenu){
              //cout << "newmenu\n";
              domenu(thismenu->newmenu);
              //cout << "afternewmenu\n";
              resetmenu(rootmenu);
              thismenu=rootmenu;
              place=1;
              dest=1;
            }else{
              //cout << "end\n";
              resetmenu(rootmenu);
              exit=0;
            }
          }else{
            //cout << "cancel\n";
            doactions(thismenu->cancel);
            thismenu=rootmenu;
            place=1;
            dest=1;
          }
        }     // EndIf confirm.
      }       // EndIf nextmenu->selection>0.
    }else{     // ElseIf place==dest.
      exit=0;
    }
  }           // LoopWhile exit!=0.

  return 0;
}


int menu::testmenu(menuitem *rootmenu)
{
  cout << "testing menu...\n";
  menuitem *menuer=rootmenu;
  menuaction *acter;

  while(menuer!=NULL){
    cout << "title: " << "[" << menuer->title << "]" << endl;
    cout << "text: \n" << menuer->text << endl;
    cout << "cont: " << menuer->continuity << endl;
    cout << "conf: " << menuer->confirm << endl;
    cout << "max: " << menuer->max << endl;
    if(menuer->newmenu != NULL)
      cout << "newmenu: " << menuer->newmenu->title << endl;
    if(menuer->before != NULL){
      acter=menuer->before;
      while(acter!=NULL){
        cout << "action:" << acter->title << endl;
        for(int n=0; n<9; n++){
          cout << acter->num.at(n);
          cout << acter->str.at(n);
          cout << " ";
        }
        cout << endl;
        acter=acter->next;
      }
    }
    cout << endl;
    
    if(menuer->newmenu!=0)
      testmenu(menuer->newmenu);

    menuer=menuer->next;
  }
}


menuitem *menu::getmenu()
{
  return rootmenu;
}
