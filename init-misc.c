int init::findvictor()
// Returns the initiative number of the last team standing.
{
  unit *browser;
  unit *finger=NULL;

  int init=0;
  int winit=-1;
  init++;

  browser=start;
  while(browser!=NULL){
    if(browser->out==-1)
      if(browser->team!=0)
	if(finger!=NULL){
	  if(browser->team != finger->team)
	    return 0;
	}else{
	  finger=browser;
	  winit=init;
	}

    browser=browser->next;
    init++;
  }

  return winit;
}


int init::out(int index, int toggle)
// Changes one unit's "out" status.
{
  unit *browser;
  int init=0;

  init++;
  browser=start;
  while(browser!=NULL){

    if(init==index){
      browser->out=toggle;
      return 0;
    }

    browser=browser->next;
    init++;
  }
  return -1;
}


int init::output()
// Displays the entire initiative list in neat columns and rows.
{
  unit *browser;   //Pointer to browse list.
  st *finger;    //Pointer to unit stat.
  st *sever;     //Pointer to unit stat.

  browser=start;   //Point *browser to first unit.

  int rows=3;      //How many rows to display at once.
  int cols=6;      //How many columns to display per row.
  int lines=6;
  int length=10;   //How long a listing may be.

  int row = 0;     //Row integer for display.
  int col = 0;     //Column integer for display.
  int line = 0;    //Line integer for display.
  int stat = 0;    //Stat integer for display. 
  int n;
  int ten=10;
  int len=5;
  int init = 1;    //Initiative number.

  while(browser!=NULL){
    for(row=1; row <= rows; row++)
      if(browser!=NULL){

	for(line=1; line < lines+4; line++){

	  browser=start;
	  init = 1;
	  for(col=1; col<=cols; col++){
	    
	    while(init < (row-1)*cols+col){
	      if(browser!=NULL)
		browser=browser->next;
	      init++;
	    }

	    if(browser!=NULL)
	      if(line==1)
		cout << "Init: " << init << "   ";
	      else if(line==2)
                cout << "Nick: " << browser->title << "   ";

	      else if(line==3)
		cout << "Pos : " << browser->x << "," << browser->y << " ";

	      else if(line==4)
		cout << "Team: " << browser->team << "   ";

	      else if(line>4){
		finger=browser->stats;
		for(stat=0; stat<line-5; stat++)
		  if(finger!=NULL)
		    finger=finger->next;

		if(finger!=NULL){

		  len=6;
		  ten=10;
		  while(ten <= finger->value[1]){
		    len++;
		    ten*=10;
		  }

		  for(n=0; n<3; n++)
		    cout << finger->title[n];

		  cout << ": " << finger->value[1];

		  if(length > len)
		    for(n=0; n < (length-len); n++)
		      cout << " ";
		}
	      }

	  }

	cout << endl;
	}

      }

  }

}


int init::getunits(vector<int> units, string stats, string &ulist)
{
  ulist.clear();
  ostringstream list;
  list.str(ulist);
  
  unit *browser;
  st *finder;
  string title;
  int s=0, s2=0;
  int init=1, u=0;
  string strinit="init";
  string strtitle="title";
  string strpos="pos";
  string strteam="team";
  
  browser=start;
  while(u < units.size()){
    
    if(units[u]<init && units[u]!=0 /*|| browser==NULL*/){
      browser=start;
      init=1;
    }else if(browser==NULL && units[u]>init){
      browser=start;
      init=1;
      u++;
    }

    if(browser!=NULL){
      if(units[u]==init || units[u]==0){

        s=0;
        while(s!=string::npos){
          s=stats.find_first_not_of(" ", s);
          if(s!=string::npos){
            s2 = stats.find(" ", s);
            if(s2!=string::npos)
              title.assign(stats, s, s2-s);
            else
              title.assign(stats, s, stats.length()-s);

            //cout << title;
            if(title == "init")
              list << "\"init: " << init << " \"";
            else if(title == "title")
              list << "\"title: " << browser->title << " \"";
            else if(title == "pos")
              list << "\"pos: " << browser->x << "," << browser->y << " \"";
            else if(title == "team")
              list << "\"team: " << browser->team << " \"";
            else{
              finder=browser->stats;
              while(finder!=NULL){
                if(finder->title == title)
                  list << "\"" << finder->title << ": " << finder->value[1] << "/" << finder->value[2] << " \"";
                finder=finder->next;
              }
            }

            s=s2;
            if(s!=string::npos)
              s=stats.find(" ", s);

          }
        }

        list << "*";
        if(units[u]!=0 || units[u]==0 && browser->next==NULL)
          u++;
      }
      browser=browser->next;
      init++;
    }
  }

  ulist=list.str();
  return 0;
}


int init::unitexist(int index)
{
  unit *browser;
  int initnum=1;

  browser=start;
  while(browser!=NULL){
    if(initnum==index)
      return true;

    browser=browser->next;
    initnum++;
  }

  return false;
}


int init::getavatar(int index)
{
  unit *browser;
  int initnum=1;

  browser=start;
  while(browser!=NULL){
    if(initnum==index)
      return browser->avatar;
    browser=browser->next;
    initnum++;
  }

  return false;
}


int init::move(int index, int x, int y)
{
  unit *browser;
  unit *finger=new unit;
  int init = 1;

  browser=start;
  
  while(browser!=NULL){
    if(init==index){
      delete finger;
      finger=browser;
    }
    
    if(browser->x==x && browser->y==y){
      cout << "Error: Position occupied. Unit not moved.\n";
      return -1;
    }

    browser=browser->next;
    init++;
  }

  finger->x=x;
  finger->y=y;
  return 0;
}


int init::getnick(int index, char *title)
{
  int init;

  unit *browser;

  browser=start;
  init=1;

  while(browser!=NULL){
    if(init==index){
      browser->title=title;
      return true;
    }

    browser=browser->next;
    init ++;
  }
}



int init::getteam(int index)
{
  unit *browser;
  int init=1;

  browser=start;
  while(init < index){
    browser=browser->next;
    init++;
  }

  if(init==index)
    return browser->team;
}



int init::getxy(int index, int *x, int *y)
{
  unit *browser;
  int init=1;

  browser=start;

  while(browser!=NULL){
    if(init==index){
      *x=browser->x;
      *y=browser->y;
      return 0;
    }
    
    browser=browser->next;
    init++;
  }

  return -1;
}
