#include <ncurses.h>


WINDOW *wmenu;     // Declaring pointers for each different window.
WINDOW *wmesg;
WINDOW *wmap;
WINDOW *wunits;
WINDOW *wconfirm;
WINDOW *debug;

class display_ncurses {
 public:
  int matrix(int x, int y, int ox, int oy, int size, int range, int occ, int unocc, int team);
  int menu(char title[], char menu[], int cursor, int max);
  int message(char mesg[]);
  int confirm();
  int cancel();
  int units(int *units);
  int drawall();

  display_ncurses();
  ~display_ncurses();
};


display_ncurses::display_ncurses()
{
  initscr();

  debug = newwin(5, 5, 70, 5);
  box(debug, 0, 0);
  wmenu = newwin(15, 15, 50, 20);     // Declaring windows for each window pointer.
  box(wmenu, 0, 0);
  wmesg = newwin(5, 30, 70, 20);
  box(wmesg, 0, 0);
  wmap = newwin(45, 48, 5, 0);
  box(wmap, 0, 0);
  wunits = newwin(36, 79, 5, 48);
  box(wunits, 0, 0);
  wconfirm = newwin(5, 30, 75, 20);
  box(wconfirm, 0, 0);
}


display_ncurses::~display_ncurses()
{
  endwin();     // Stop ncurses mode.
}


int display_ncurses::matrix(int x, int y, int ox, int oy, int size, int range, int occ, int unocc, int team)
{
  wclear(wmap);
  box(wmap, 0, 0);

  int sx, sy;
  int dist, distx, disty;
  int ally;
  char nick[8];

  int matrix[size][size];
  int init=1;

  for(sy=0; sy<size; sy++){
    for(sx=0; sx<size; sx++){
      matrix[sx][sy] = 0;              // Change all elements to 0;
    }
  }

  while(init_getxy(init, &sx, &sy)){
    if(sx>=ox && sy>=oy)
      if(sx < ox+size  &&  sy < oy+size)
	matrix[sx-ox][sy-oy] = init;     // Put units on the matrix.
    init++;
  }

  for(sx=0; sx<size; sx++)               // Print x coordinates across the top of the grid.
    mvwprintw(wmap, 0, sx*3+2, "%d", sx+ox);

  for(sy=0; sy<size; sy++){
    mvwprintw(wmap, sy+1, 0, "%d", sy+oy);
    for(sx=0; sx<size; sx++){

      init=matrix[sx][sy];

      dist = distance(x-ox, y-oy, sx, sy);

      if(init==0)
	if(unocc==0 && dist<=range)
	  wprintw(wmap, "[ ]");
	else
	  wprintw(wmap, "   ");
      else if(init_getteam(init) == team)
	if(occ==1 && dist<=range)
	  wprintw(wmap, "[%d]", init);
	else
	  wprintw(wmap, " %d ", init);
      else if(init_getteam(init) != team)
	if(occ==-1 && dist<=range)
	  wprintw(wmap, "[%d]", init);
	else
	  wprintw(wmap, " %d ", init);
      else
	wprintw(wmap, " %d ", init);
    }
  }

  wrefresh(wmap);
}


int display_ncurses::menu(char title[], char menu[], int cursor, int max)
{
  wclear(wmenu);
  box(wmenu, 0, 0);

  char m;
  int line=1, l=0;

  mvwprintw(wmenu, 0, 0, "[%s]", title);
  wprintw(wmenu, "%d", cursor);
  while(strcmp(&menu[l], "\0")!=0){
    if(strcmp(&menu[l], "\n")!=0){
      mvwprintw(wmenu, cursor, 0, "->");
      wprintw(wmenu, "%d: ", line);
      while(strcmp(&menu[l], "\n")!=0 && strcmp(&menu[l], "\0")!=0){
	waddch(wmenu, menu[l]);
        l++;
      }
      cout << endl;
      line++;
      if(strcmp(&menu[l], "\n")==0)
        l++;
    }
  }

  //mvwaddstr(wmenu, 1, 2, menu);

  wrefresh(wmenu);
}


int display_ncurses::message(char mesg[])
{
  wclear(wmesg);
  box(wmesg, 0, 0);

  mvwaddstr(wmesg, 1, 1, mesg);

  wrefresh(wmesg);
}


int display_ncurses::confirm()
{
  wclear(wconfirm);

  mvwprintw(wconfirm, 1, 1, "[ENTER]:Confirm");

  wrefresh(wconfirm);
}


int display_ncurses::cancel()
{
  mvwprintw(wconfirm, 1, 17, "[BACKSPACE]:Cancel");

  wrefresh(wconfirm);
}


int display_ncurses::units(int *units)
{
  wclear(wunits);
  box(wunits, 0, 0);

  unit *browser;   //Pointer to browse list.
  stat *finder;    //Pointer to unit stat.

  int rows=3;      //How many rows to display at once.
  int cols=6;      //How many columns to display per row.
  int lines=10;     //How many lines per listing.
  int length=10;   //How long a listing may be.

  int row = 0;     //Row integer for display.
  int col = 0;     //Column integer for display.
  int line = 0;    //Line integer for display.
  int stat = 0;    //Stat integer for display. 
  int n;
  int u = 0;
  int ten=10;
  int len=5;
  int init = 1;    //Initiative number.

  browser=start;   //Point *browser to first unit.
  while(browser!=NULL){
    for(row=0; row<=rows; row++)
      if(browser!=NULL)
	for(line=1; line<=lines; line++){
	  browser=start;
	  init=1;
	  for(col=1; col<=cols; col++){
	    if(units[u] != 0)
	      u = ((row*cols)+col) - 1;
	    while(init != units[u]  &&  units[u] > 0  ||  init != (row*cols)+col  &&  units[u] == 0){
	      if(init > units[u])
		if(init > (row*cols)+col  ||  units[u] > 0){
		  browser=start;
		  init=1;
		}
	      if(init < (row*cols)+col  &&  units[u] == 0  ||  init < units[u]  &&  units[u] != 0){
   		  if(browser!=NULL)
		    browser=browser->next;
		  init++;
	      }
	    }
	    if(browser!=NULL){
	      if(line==1)
		mvwprintw(wunits, row*lines+line+1, (col-1)*length+1, "Init: %d", init);
	      else if(line==2)
		mvwprintw(wunits, row*lines+line+1, (col-1)*length+1, "Nick: %s", browser->nick);
	      else if(line==3)
		mvwprintw(wunits, row*lines+line+1, (col-1)*length+1, "Pos : %d,%d", browser->x, browser->y);
	      else if(line==4)
		mvwprintw(wunits, row*lines+line+1, (col-1)*length+1, "Team: %d", browser->team);
	      else{
		finder=browser->stats;
		for(stat=0; stat < line-4; stat++)
		  if(finder!=NULL)
		    finder=finder->next;
		if(finder!=NULL){
		  mvwaddstr(wunits, row*lines+line+1, (col-1)*length+1, finder->title);
		  wprintw(wunits, ": %d/%d", finder->value[1], finder->value[2]);
		}
	      }
	    }
	    if(units[u] > 0)
	      u++;
	    if(units[u] < 0)
	      browser=NULL;
	  }
	  cout << endl;
	}
  }

  wrefresh(wunits);
}


int display_ncurses::drawall()
{
  wrefresh(wmap);

  //wrefresh(wmenu);
  //wclear(wmenu);

  //wrefresh(wmesg);
  //wclear(wmesg);

  //wrefresh(wunits);
  //wclear(wunits);
}
