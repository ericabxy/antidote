int distance(int x, int y, int x2, int y2);

#include "init.h"
#include "io.h"
#include <iostream>

using namespace std;

int debug()
{
  int x, y, n;
  int units, teams;

  /*cout << "Enter number of units in battle: ";
  cin >> units;
  cout << "Enter number of teams in battle: ";
  cin >> teams;*/

  units = 6;
  teams = 2;

  for(n=1; n<=units; n++){
    y = n/8+1;
    x = y*8-n;
    init_addunit(0, n+64, x, y, (n+2)/(units/teams));
    init_addstat(n, "balance", 0, 10, 10);
    init_addstat(n, "time", 0, 4, 4);
    init_addstat(n, "move", 3, 3, 3);
    init_addstat(n, "range", 6, 6, 6);
  }
  init_output();
}

int clock_tick()
{
  int var;

  int yourturn, yourmenu=0;        // Init of current unit, first menu selection.
  int move;                        // Move range;
  int x=0, y=0, yourx=0, youry=0;  // Selected x y, current x y.
  int target=0, power=0, re;       // Target initnum, attack power, initlist browser.
  int range, tx, ty;               // Attack range, target x y.
  int teams, units, n;             // Number of teams, number of units, n variable.
  int victor, team;                // Init of victor, team number of current unit.
  int confirm=0;                   // Confirmation variable.
  char nick[8];                    // Name of current unit.
  char *mesg[256];                 // Message array.


  display_iostream display;
  control_iostream control;


  while(init_victory() < 1){
    if(init_victory() == 0){

      yourturn = init_findunit("balance", 10, false);     // Find the first unit with a balance of 10.
      if(yourturn == 0)
	init_modstat(0, "balance", 1, +1, false);         // If there is none, raise everyone's balance by 1.

      else{
	init_getxy(yourturn, &yourx, &youry);             // Get the unit's x and y coordinates.
	team = init_getteam(yourturn);                    // Get the unit's team.
	move = 3;                                         // Get the unit's move range.
	range = 6;                                        // Get the unit's attack range.
	//init_getnick(yourturn, &nick);
	//cout << "nick: " << nick << endl;

	
        // Menu system for controlling the current unit.


	if(confirm != 0){
	  if(yourmenu > 0){
	    if(x > 0){
	      display.matrix(yourx, youry, 1, 1, 16, move, 0, 0, team);
	      cout << "Pos: " << yourx << "," << youry << endl;

	      display.menu("move", "", 1, 1);
	      display.message("x: ");
 	      control.input(&x, 0);

	      display.message("y: ");
	      control.input(&y, 0);

	      display.message("");
	      cout << "Newpos: " << x << "," << y << endl;
	    }else if(target > 0){
	      if(power > 0){
		display.menu("attack power", "1\n2\n3\n", 1, 3);
		display.message(":");
		control.input(&power, 3);
	      }else{
		display.matrix(yourx, youry, 1, 1, 16, range, -1, -1, team);
		//init_output();
		display.menu("target", "", 1, 1);
		display.message(":");
		control.input(&target, 0);
	      }
	    }else{
	      cout << endl;
	      init_output();
	      cout << yourturn << "'s turn!\n";
	      display.menu("yourturn", "1: move\n2: attack\n", 1, 3);
	      display.message(":");
	      control.input(&yourmenu, 2);
	    }
	  }
	  cout << endl;
	}
	display.confirm();
	confirm = control.confirm();
	display.drawall();


        /* What to do when the user enters "confirm" input. */


	if(confirm == 0){
	  if(yourmenu > 0){
	    if(x > 0){
	      if(distance(yourx, youry, x, y) <= move)
		init_move(yourturn, x, y);
	      else
		cout << "!! Error: distance exeeds move range\n";
	      display.matrix(x, y, 1, 1, 16, 3, 2, -1, team);
	      yourmenu = 0;
	      x = 0;
	      y = 0;
	      target = 0;
	      power = 0;
	      for(n=0;n<10;n++)
		cout << endl;
	    }else if(target > 0){
	      init_getxy(target, &tx, &ty);
	      if(distance(yourx, youry, tx, ty) <= range)
		if(power > 0){
		  re = 1;
		  while(init_modstat(re, "balance", false, 1, 0)){
		    if(re != yourturn && re != target)
		      init_modstat(re, "balance", false, 1, +power);
		    else
		      init_modstat(re, "balance", false, 1, -power);
		    re++;
		  }
		init_output();
		yourmenu = 0;
		x = 0;
		y = 0;
		target = 0;
		power = 0;
		for(n=0;n<10;n++)
		  cout << endl;
		}else
		  power = 1;
	      else
		cout << "!! Error: target distance exceeds attack range\n";
	    }else if(yourmenu == 1){
	      x = yourx;
	      y = youry;
	    }else if(yourmenu == 2){
	      target = 1;
	    }else
	      cout << "!! Error: invalid selection\n";
	  }else{
	    yourmenu = 1;
	  }
	  confirm = -1;
	}
      }
    }else{
      if(confirm != 0){
	if(units > 0){
	  if(teams > 0){
	    display.menu("teams", " ", 1, 1);
	    display.message("Enter the number of teams: ");
	    control.input(&teams, 0);
	  }else{
	    display.menu("units", " ", 1, 1);
	    display.message("Enter the number of units: ");
	    control.input(&units, 0);
	  }
	}else
	  teams = 0;
      }
      if(confirm == 0){
	if(units > 0){
	  if(teams > 0)
	    for(n = 1; n < units; n ++){
	      y = n/8+1;
	      x = y*8-n;
	      init_addunit(0, n+64, x, y, n/(units/teams));
	      init_addstat(n, "balance", 0, 10, 10);
	    }
	  else
	    teams = 1;
	}else{
	  units = 2;
	  teams = 0;
	}
	confirm = false;
      }
    }
  }
}


int distance(int x, int y, int x2, int y2)
{
  int distx;
  int disty;

  distx = x-x2;
  disty = y-y2;
  if(distx > 0)
    if(disty > 0)
      return distx+disty;
    else
      return distx-disty;            // Calculate the square distance to each coordinate.
  else if(disty > 0)
    return disty-distx;
  else
    return -disty-distx;
}


int main()
{
  
  debug();  // Runs the debug function for testing without file input.
  while(true){
    clock_tick();
  }
}
