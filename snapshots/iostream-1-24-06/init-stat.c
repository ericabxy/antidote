int init_addstat(int index, char *title, int min, int cur, int max)
{
  unit *browser;
  stat *finder;

  int init=0;
  if(index)
    init++;

  browser=start;
  while(browser!=NULL){
    if(init==index){
      
      if(browser->stats!=NULL){
	finder=browser->stats;
	while(finder->next!=NULL){
	  finder=finder->next;
	}
	finder->next=new stat;
	finder=finder->next;
	finder->next=NULL;
      }else{
	finder=new stat;
	finder->next=NULL;
	browser->stats=finder;
      }

      while(finder!=NULL){
	if(finder->next==0){
	  strcpy(finder->title, title);
	  finder->value[0]=min;
	  finder->value[1]=cur;
	  finder->value[2]=max;
	  if(finder->value[1] < finder->value[0])
	    finder->value[1] = finder->value[0];
	  if(finder->value[1] > finder->value[2])
	    finder->value[1] = finder->value[2];
	  break;
	}else{
	  finder=finder->next;
	}
      }

    }
    browser=browser->next;
    if(index)
      init++;
  }
}


int init_delstat()
{
}


int init_findunit(char *title, int key, int absolute)
{
  unit *browser;
  unit *finger=new unit;
  stat *finder;
  stat *sever=new stat;

  int init=0;
  int winit;
  init++;

  browser=start;
  while(browser!=NULL){

    if(browser->out){
      if(browser->stats!=NULL){
	finder=browser->stats;
	while(finder!=NULL){
	  if(strcmp(finder->title, title) == 0)
	    if(finder->value[1] > sever->value[1]){
	      sever=finder;
	      winit=init;
	    }
	  finder=finder->next;
	}
      }
    }
    if(browser->next==NULL)
      return winit;

    browser=browser->next;
    init++;
  }
}


int init_getstat()
{
}


int init_modstat(int index, char *title, int absolute, int element, int factor)
{
  unit *browser;
  stat *finder;
  stat *sever;

  int init=0;
  if(index)
    init++;

  browser=start;
  while(browser!=NULL){
    if(init==index){

      if(browser->stats!=NULL){
	finder=browser->stats;
	while(finder!=NULL){
	  if(strcmp(finder->title, title) == 0){
	    if(absolute)
	      finder->value[element]=0;
	    finder->value[element]+=factor;
	    if(finder->value[1] < finder->value[0])
	      finder->value[1] = finder->value[0];
	    if(finder->value[1] > finder->value[2])
	      finder->value[1] = finder->value[2];
	  }
	  finder=finder->next;
	}
      }
    }

    if(browser->next!=NULL){
      browser=browser->next;
      if(index)
	init++;
    }else if(index > init){
      cout << "!!Error: Unit not found.\n";
      return false;
    }else
      return true;
  }
}
