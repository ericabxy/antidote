int init_victory()
{
  return 0;
}

int init_output2()
{
  unit *browser;
  unit *finger;

  browser=start;

  while (browser!=NULL){
    cout << "Nick: " << browser->nick << endl;
    cout << "Balance: " << endl;
    cout << "Pos : " << browser->x << "," << browser->y << endl;
    cout << "Team: " << browser->team << endl;
    cout << endl;
    
    browser=browser->next;
  }
}

int init_output()
// Displays the entire initiative list in neat columns and rows.
{
  unit *browser;   //Pointer to browse list.
  unit *finger;    //Pointer to indexed unit.
  stat *finder;     //Pointer to unit stat.

  browser=start;   //Point *browser to first unit.

  int rows=3;        //How many rows to display at once.
  int cols=6;    //How many columns to display per row.
  int lines=6;
  int length=13; //How long a listing may be.

  int row = 0;     //Row integer for display.
  int col = 0;     //Column integer for display.
  int line = 0;    //Line integer for display.
  int stat = 0;    //Stat integer for display. 
  int n;
  int ten;
  int len;
  int init = 1;    //Initiative number.

  while(browser!=NULL){
    for(row=1; row <= rows; row++)
      if(browser!=NULL){

	for(line=1; line < lines+4; line++){

	  browser=start;
	  init = 1;
	  for(col=1; col<=cols; col++){
	    
	    while(init < (row-1)*cols+col){
	      if(browser!=NULL)
		browser=browser->next;
	      init++;
	    }

	    if(browser!=NULL)
	      if(line==1)
		cout << "Init: " << init << "   ";

	      else if(line==2)
		cout << "Nick: " << browser->nick << "   ";

	      else if(line == 4)
		cout << "Pos : " << browser->x << "," << browser->y << " ";

	      else if(line == 5)
		cout << "Team: " << browser->team << "   ";

	      else if(line < 2+lines){
		finder=browser->stats;
		for(stat=0; stat < lines-2; stat++)
		  if(finder!=NULL)
		    finder=finder->next;
		if(finder!=NULL)
		  cout << finder->title << ": " << finder->value[1];
	      }

	  }

	cout << endl;
	}

      }

  }

}


int init_move(int index, int x, int y)
{
  unit *browser;
  unit *finger=new unit;
  int init = 1;

  browser=start;
  
  while(init<=index){
    if(init==index){
      delete finger;
      finger=browser;
    }
    
    if(browser->x==x && browser->y==y){
      cout << "Error: Position occupied. Unit not moved.\n";
      return false;
    }

    if(browser->next!=NULL){
      browser=browser->next;
      init++;
    }
  }

  /*if(finger->init){
    cout << "Error: Unit not found.\n";
    return false;
    }*/

  finger->x=x;
  finger->y=y;
  return true;
}


int init_getnick(int index, char *nick)
{
  int init;

  unit *browser;

  browser=start;
  init=1;

  while(browser!=NULL){
    if(init==index){
      strcpy(browser->nick, nick);
      return true;
    }

    browser=browser->next;
    init ++;
  }
}



int init_getally(int index, int team)
{
  unit *browser;
  int init=1;

  browser=start;
  while(init!=team){
    browser=browser->next;
    init++;
  }

  if(init==team)
    if(init+team)
      return false;
  
  return (init==team);
}



int init_getxy(int index, int *x, int *y)
{
  unit *browser;
  int init=1;

  browser=start;

  while(browser->next != NULL){
    if(init==index){
      *x=browser->x;
      *y=browser->y;
      return true;
    }
    
    browser=browser->next;
    init++;
  }

  return false;
}


